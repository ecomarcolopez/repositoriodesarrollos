﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AttachInvoice.aspx.cs" Inherits="AttachInvoice" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Asociar Factura</title>

    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />

    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/select.dataTables.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/buttons.dataTables.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->


</head>
<body style="background-size: 100%; background-repeat: no-repeat;">
    <form id="form1" runat="server">
        <div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="Images/logo.png" height="35" /></span>  Control de Pago a proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="UserHome.aspx">Inicio</a></li>
                            <li class="active"><a href="#">Asociar Facturas</a></li>
                            <li>&nbsp;&nbsp;<asp:Button ID="btnSignOut" runat="server" CssClass="btn btn-default navbar-btn" Text="Cerrar Sesión" OnClick="btnSignOut_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--Grid View-->

        <div class="container" style="padding-top: 5%">
            <div id="divContainer" runat="server">
            </div>
            <br />
            <%--<div class="events"></div>--%>
        </div>

    </form>
</body>

<script src="Scripts/jquery-3.2.1.min.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/DataTables/jquery.dataTables.js"></script>
<script src="Scripts/DataTables/dataTables.bootstrap.min.js"></script>
<script src="Scripts/DataTables/jquery.dataTables.min.js"></script>
<script src="Scripts/DataTables/dataTables.select.min.js"></script>
<script src="Scripts/DataTables/buttons.print.min.js"></script>
<script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
<script src="Scripts/jszip.min.js"></script>
<script src="Scripts/pdfmake/pdfmake.min.js"></script>
<script src="Scripts/pdfmake/vfs_fonts.js"></script>
<script src="Scripts/DataTables/buttons.html5.min.js"></script>
<script>

    window.onload = function () {

        var events = $('.events');
        var mytable = $('#gvRecibos').DataTable({

            pageLength: 10,
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'copy',
                    text: 'Copiar', 
                    exportOptions: {
                        rows: { selected: true }
                    }
                },
                {
                    extend: 'pdf',
                    text: 'PDF',
                    exportOptions: {
                        rows: { selected: true }
                    }
                },
                {
                    extend: 'excel',
                    text: 'Excel',
                    exportOptions: {
                        rows: { selected: true }
                    }
                },
                {
                    text: 'Get selected data',
                    action: function () {


                        var data = mytable.rows({ selected: true }).data();
                        var newarray = [];
                        for (var i = 0; i < data.length ; i++) {
                            events.prepend('<div>' + data[i][1] + data[i][2] + data[i][3] + data[i][4] + data[i][5] + data[i][6] + '</div>');
                            newarray.push(data[i][1]);
                            newarray.push(data[i][2]);
                            newarray.push(data[i][3]);
                            newarray.push(data[i][4]);
                            newarray.push(data[i][5]);
                            newarray.push(data[i][6]);
                            //alert("Name: " + data[i][0] + " Address: " + data[i][1] + " Office: " + data[i][2]);
                        }

                        var sData = newarray.join();
                        alert(sData);


                        //Saber cuantas filas estan seleccionadas
                        //var count = mytable.rows({ selected: true }).count();
                        //events.prepend('<div>' + count + ' row(s) selected</div>');
                    }
                }
            ],
            language: { //Cambio de lenguaje, no hay uno predefinido, tienes que personalizar tus mensajes
                "lengthMenu": "Mostrando _MENU_ registros por pagina",//Indica cantidad de paginas mostradas
                "zeroRecords": "No se encontraron registros",//Mensaje que se desplegará su una busqueda no produce resultados
                "info": "Mostrando página _PAGE_ de _PAGES_",//Indica cuantas paginas de el total de estas mostrando
                "infoEmpty": "No registros disponibles",//Mensaje que se desplegará cuando no haya información en la tabla
                "infoFiltered": "(filtrados de un total de  _MAX_ )",//Indica cuanto resultados se obtuvieron de filtrar por medio de una busqueda
                "search": "Buscar en cualquier columna:",//Mensaje de label en Buscar
                "paginate": {
                    "previous": "Anterior",
                    "next": "Siguiente"
                },
                "select": {
                    rows: {
                        _: "%d recibos seleccionados",
                        0: "ctrl + click izquierdo para seleccionar mas de un recibo",
                        1: "1 recibo seleccionado"
                    }
                }

            },
            "aaSorting": [0, 'desc'],
            columnDefs: [{
                orderable: false,
                className: 'select-checkbox',
                targets: 0
            }],
            select: {
                style: 'os',
                selector: 'td:first-child'
            },
            order: [[1, 'asc']]

        });
    };
</script>
</html>

