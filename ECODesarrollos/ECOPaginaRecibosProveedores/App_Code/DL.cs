﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Libraries;
using System.Configuration;

/// <summary>
/// Summary description for DL
/// </summary>
public class DL
{

    String Conexiones = ConfigurationManager.ConnectionStrings["ECOPaginaRecibosProveedoresConnectionString1"].ConnectionString;

    public DataTable GetRFCExistente(string RFC)
    {
        DataTable dt;
        string Query = "dbo.ECO_GetRFCExistente";
        SqlParameter[] Par = new SqlParameter[1];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;
        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];
        return dt;
    }

    public DataTable SetNewUser(string RFC,string Pwd,string Email)
    {
        DataTable dt;
        string Query = "dbo.ECO_SetNewUser";
        SqlParameter[] Par = new SqlParameter[3];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;
        Par[1] = new SqlParameter("@Pwd", SqlDbType.NVarChar);
        Par[1].Value = Pwd;
        Par[2] = new SqlParameter("@Email", SqlDbType.NVarChar);
        Par[2].Value = Email;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;


    }

    public DataTable ValidaEmailRFC(string RFC,string Email)
    {
        DataTable dt;
        string Query = "ECO_ValidaEmailRFC";
        SqlParameter[] Par = new SqlParameter[2];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;
        Par[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
        Par[1].Value = Email;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;
    }

    public DataTable GetUsuarioSignIn(string RFC, string Pwd)
    {
        DataTable dt;
        string Query = "ECO_GetUsuarioSignIn";
        SqlParameter[] Par = new SqlParameter[2];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;
        Par[1] = new SqlParameter("@Pwd", SqlDbType.NVarChar);
        Par[1].Value = Pwd;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query,Par).Tables[0];

        return dt;
            
    }

    public DataTable GetRFCAndEmail(string RFC,string Email)
    {
        DataTable dt;
        string Query = "dbo.ECO_GetRFCAndEmail";
        SqlParameter[] Par = new SqlParameter[2];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;
        Par[1] = new SqlParameter("@Email", SqlDbType.NVarChar);
        Par[1].Value = Email;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;
    }

    public DataTable GetForgotPassUUID(string UniqueId)
    {
        DataTable dt;
        string Query = "dbo.ECO_GetForgotPassUUID";
        SqlParameter[] Par = new SqlParameter[1];
        Par[0] = new SqlParameter("@UniqueId",SqlDbType.NVarChar);
        Par[0].Value = UniqueId;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;
    }

    public DataTable SetNewUserPass(string Pwd, string UniqueId)
    {
        DataTable dt;
        string Query = "dbo.ECO_SetNewUserPass";
        SqlParameter[] Par = new SqlParameter[2];
        Par[0] = new SqlParameter("@Pwd", SqlDbType.NVarChar);
        Par[0].Value = Pwd;
        Par[1] = new SqlParameter("@UniqueId", SqlDbType.NVarChar);
        Par[1].Value = UniqueId;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;
    }

    public DataTable GetRecibosProveedor(string RFC)
    {
        DataTable dt;
        string Query = "dbo.ECO_GetRecibosProveedor";
        SqlParameter[] Par = new SqlParameter[2];
        Par[0] = new SqlParameter("@RFC", SqlDbType.NVarChar);
        Par[0].Value = RFC;

        dt = SqlHelper.ExecuteDataset(Conexiones, CommandType.StoredProcedure, Query, Par).Tables[0];

        return dt;
    }
}