﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UserHome.aspx.cs" Inherits="UserHome" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Iniciar Sesión</title>

    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet" />


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->


</head>
<body style="background-image: url('Images/FondoInicio.jpg'); background-size: 100%; background-repeat: no-repeat;">
    <form id="form1" runat="server">
        <div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="Images/logo.png" height="35" /></span>  Control de Pago a proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="#">Inicio</a></li>
                            <li><a href="AttachInvoice.aspx">Asociar Facturas</a></li>
                            <li>
                                &nbsp;&nbsp;<asp:Button ID="btnSignOut" runat="server" CssClass="btn btn-default navbar-btn" Text="Cerrar Sesión" OnClick="btnSignOut_Click" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Asociar Factura-->
        <div class="row center-page">

            <h4><font color="white">Bienvenido</font></h4>
            <hr />
            <asp:Label ID="lblSuccess" runat="server" CssClass="text-success" ForeColor="White"></asp:Label>

        </div>
        <!--Asociar Factura-->

    </form>
</body>

<script src="Scripts/jquery-3.2.1.min.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/DataTables/jquery.dataTables.js"></script>
<script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
<script src="Scripts/jszip.min.js"></script>
<script src="Scripts/pdfmake/pdfmake.min.js"></script>
<script src="Scripts/pdfmake/vfs_fonts.js"></script>
<script src="Scripts/DataTables/dataTables.bootstrap.min.js"></script>
<script src="Scripts/DataTables/buttons.html5.min.js"></script>
<script src="Scripts/DataTables/buttons.print.min.js"></script>
</html>
