﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.Services;
using System.Data;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnSignIn_ServerClick(object sender, EventArgs e)
    {
        if(txtRFC.Text == "" || txtContraseña.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltan Datos')", true);
        }

        if(txtRFC.Text != "" && txtContraseña.Text != "")
        {
            DL dl = new DL();
            DataTable dt = new DataTable();

            dt = dl.GetUsuarioSignIn(txtRFC.Text, txtContraseña.Text);

            string resul = "";
            resul = dt.Rows[0][0].ToString();

            if (resul == "Correcto")
            {
                Session["UserRFC"] = txtRFC.Text;
                Response.Redirect("~/UserHome.aspx");
            }
            if(resul == "Incorrecto")
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Usuario o contraseña incorrectos')", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Hay un problema al iniciar sesión')", true);
            }
        }

    }
}