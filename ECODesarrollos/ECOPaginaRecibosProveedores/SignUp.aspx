﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SignUp.aspx.cs" Inherits="SignUp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Registrar</title>

    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->

</head>
<body style="background-image:url('Images/FondoInicio.jpg');background-size: 100%; background-repeat: no-repeat; ">
    <form id="form1" runat="server">
        <div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="Images/logo.png" height="35"/></span>  Control de Pago a proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Iniciar Sesión</a></li>
                            <li class="active"><a href="#">Registrarse</a></li>
                            <li><a href="ForgotPassword.aspx">Olvidé Contraseña</a></li>
                            <li><a href="#">Acerca de</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Sign UP--> 
        <div class="row center-page">

            <h4><font color="white">Registrarse</font></h4>
            <hr />

            <div class="col-md-1"></div>
            <div class="col-md-8">

                <asp:Label runat="server" ForeColor="White">RFC</asp:Label>
                <div>
                    <asp:TextBox ID="txtRFC" Style="width: 60%" runat="server" Class="form-control" placeholder="Ingrese su RFC"></asp:TextBox>

                </div>


                <asp:Label runat="server" ForeColor="White">Correo electrónico</asp:Label>
                <div>
                    <asp:TextBox ID="txtEmail" Style="width: 60%" runat="server" Class="form-control CamposPage" placeholder="Correo Electronico" TextMode="Email"></asp:TextBox>
                </div>

                <asp:Label runat="server" ForeColor="White">Contraseña</asp:Label>
                <div>
                    <asp:TextBox ID="txtContraseña" Style="width: 80%" runat="server" Class="form-control CamposPage" placeholder="Ingrese su contraseña" TextMode="Password"></asp:TextBox>
                </div>

                <asp:Label runat="server" ForeColor="White">Confirmar Contraseña</asp:Label>
                <div>
                    <asp:TextBox ID="txtConfirmarContraseña" Style="width: 80%" runat="server" Class="form-control CamposPage" placeholder="Confirmar contraseña" TextMode="Password"></asp:TextBox>
                </div>
                <div>
                    <br />
                    <%--<asp:Button ID="btnSignUp" runat="server" class="btn btn-success" Text="Registrar" OnClick="btnSignUp_CLick" />--%>
                    <button type="button" class="btn btn-md btn-success" runat="server" id="btnSignUp" onclick="SignUp()">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp;Registrar</button>
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="White" Width="487px"></asp:Label>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">¿Cómo me registro?</h4>
                            </div>
                            <div class="modal-body">
                                <p><b>Por cuestiones de seguridad:</b></p>
                                <p>Para que se pueda registrar es necesario que el RFC que proporcione sea el mismo con el que Grupo ECO lo tiene 
                                    dado de alta como proveedor, al igual el correo debe ser alguno que nos haya proporcionado.</p>
                                <p>Si considera que los datos que esta poniendo son correctos, favor de comunicarse al (Pendiente)</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            </div>
                        </div>

                    </div>
                </div>


            </div>

            <div class="Info-RFC">
                <!-- Trigger the modal with a button -->
                <a id="myBtn" href="#myModal" style="color: #fff">Ayuda</a>
                <div class="col-md-3"></div>
            </div>
            
        </div>
        <!--Sign UP-->

    </form>
    </body>

    <script src="Scripts/jquery-3.2.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/DataTables/jquery.dataTables.js"></script>
    <script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
    <script src="Scripts/jszip.min.js"></script>
    <script src="Scripts/pdfmake/pdfmake.min.js"></script>
    <script src="Scripts/pdfmake/vfs_fonts.js"></script>
    <script src="Scripts/DataTables/dataTables.bootstrap.min.js"></script>
    <script src="Scripts/DataTables/buttons.html5.min.js"></script>
    <script src="Scripts/DataTables/buttons.print.min.js"></script>

        <script>

        $(document).ready(function () {
            $("#myBtn").click(function () {
                $("#myModal").modal();
                $('#txtEmail').popover("hide");
                $('#txtRFC').popover("hide");
            });
        });

        function ValidaEmail(emailToEvaluate) {

            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(emailToEvaluate)) {

                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {

            $('#txtEmail').blur(function () {


                var txtRFC = document.getElementById('txtRFC');
                var txtEmail = document.getElementById('txtEmail');

                txtEmail.value = txtEmail.value.trim();
                txtRFC.value = txtRFC.value.trim();

                var Parametro = "{RFC:'" + txtRFC.value + "',Email:'" + txtEmail.value + "'}";



                $.ajax({
                    type: "POST",
                    url: "SignUp.aspx/ValidaEmailRFC",
                    data: Parametro,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var result = response.d;

                        if (result === "Si") {
                            document.getElementById("txtEmail").style.border = "";
                            //txtEmail.setAttribute("style", "background-color:#89e5ac;width:60%");
                            $('#txtEmail').popover("hide");
                            document.getElementById("btnSignUp").disabled = false;
                        }
                        else {
                            document.getElementById("txtEmail").style.border = "thick solid #ce4444";
                            //txtEmail.setAttribute("style", "background-color:rgb(229, 123, 96);width:60%");
                            $('#txtEmail').popover("show");
                            document.getElementById("btnSignUp").disabled = true;

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }

                });


                $('#txtEmail').popover({
                    trigger: 'manual',
                    placement: 'right',
                    content: function () {
                        var message = "Correo no encontrado en base de datos";
                        return message;
                    }
                });
            });



            $('#txtRFC').blur(function () {
                var txtRFC = document.getElementById('txtRFC');
                var Parametro = "{RFC:'" + txtRFC.value + "'}";

                $.ajax({
                    type: "POST",
                    url: "SignUp.aspx/ConsultarRFC",
                    data: Parametro,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var result = response.d;

                        if (result === "Si") {
                            document.getElementById("txtRFC").style.border = "";
                            //txtRFC.setAttribute("style", "background-color:#89e5ac;width:60%");
                            $('#txtRFC').popover("hide");
                            document.getElementById("btnSignUp").disabled = false;
                        }
                        else {
                            document.getElementById("txtRFC").style.border = "thick solid #ce4444";
                            //txtRFC.setAttribute("style", "background-color:rgb(229, 123, 96);width:60%");
                            $('#txtRFC').popover("show");
                            document.getElementById("btnSignUp").disabled = true;

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }


                });

                $('#txtRFC').popover({
                    trigger: 'manual',
                    placement: 'right',
                    content: function () {
                        var message = "Tu RFC no existe en nuestra base de datos.";
                        return message;
                    }
                });

            });
        });

        function SignUp() {
            var RFC = document.getElementById('txtRFC');
            var Pwd = document.getElementById('txtContraseña');
            var PwdConfirm = document.getElementById('txtConfirmarContraseña');
            var Email = document.getElementById('txtEmail');

            txtRFC.value = txtRFC.value.trim();
            txtEmail.value = txtEmail.value.trim();

            if (RFC.value === '' || Pwd.value === '' || PwdConfirm.value === '' || Email.value === '') {
                alert('Faltan Datos');
                return;

            }

            if (Pwd.value !== PwdConfirm.value) {
                alert('Las contraseñas no coinciden');
                return;
            }

            var Parametros = "{RFC:'" + txtRFC.value + "',Pwd:'" + txtContraseña.value + "',Email:'" + txtEmail.value + "'}";
            $.ajax({
                type: "POST",
                url: "SignUp.aspx/SignUpMethod",
                data: Parametros,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var result = response.d;

                    if (result === '') {
                        result = 'Ocurrió un problema al dar de alta el usuario.';
                        return;
                    }

                    alert(result);
                    window.location.href = "../Default.aspx";


                },
                failure: function (response) {
                    alert(response.d);
                }


            });
        }
    </script>
</html>
