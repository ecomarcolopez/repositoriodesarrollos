﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class AttachInvoice : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["UserRFC"] != null)
        {
            DL dl = new DL();
            DataTable dt = new DataTable();

            string RFC = Session["UserRFC"].ToString();
            dt = dl.GetRecibosProveedor(RFC);

            ConvertDataTable2HTMLString(dt,"gvRecibos");
            divContainer.InnerHtml = ConvertDataTable2HTMLString(dt, "gvRecibos");


        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }


    public string ConvertDataTable2HTMLString(DataTable dt, string nombre)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<table id= ");

        //sb.Append('\"' + nombre + '"' + "class=\"table table-striped table-bordered dt-responsive nowrap font-size:10px cellspacing=\"0\" width=\"100 % \" \"><thead><tr>");
        sb.Append('\"' + nombre + '"' + "class=\"table table-striped table-hover  dt-responsive font-size:10px cellspacing=\"0\" width=\"100 % \" \"><thead><tr><th></th>");

        foreach (DataColumn c in dt.Columns)
        {
            sb.AppendFormat("<th>{0}</th>", c.ColumnName);
        }
        sb.AppendLine("</tr></thead><tbody>");
        int i = 0;
        foreach (DataRow dr in dt.Rows)
        {
            sb.Append("<tr class='' id= '" + i + "' ><td></td>");

            foreach (object o in dr.ItemArray)
            {
                sb.AppendFormat("<td>{0}</td>", o.ToString().Replace("\"", string.Empty));
            }
            sb.AppendLine("</tr>");
            i++;
        }
        sb.Append("</tbody><tfoot><tr>");
        sb.AppendLine("</tr></tfoot>");
        sb.AppendLine("</table>");
        string x = sb.ToString();
        return sb.ToString();
    }

    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        Session["UserRFC"] = null;
        Response.Redirect("~/Default.aspx");
    }
}