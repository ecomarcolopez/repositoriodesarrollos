﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserHome : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Session["UserRFC"] != null)
        {
            lblSuccess.Text = "Inicio de sesión correcto, bienvenido " + Session["UserRFC"].ToString() +"";
        }
        else
        {
            Response.Redirect("~/Default.aspx");
        }
    }

    protected void btnSignOut_Click(object sender, EventArgs e)
    {
        Session["UserRFC"] = null;
        Response.Redirect("~/Default.aspx");
    }
}