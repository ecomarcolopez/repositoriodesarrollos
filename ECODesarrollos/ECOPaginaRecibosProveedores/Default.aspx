﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Iniciar Sesión</title>

    <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->
    <script>

        function SignIn()
        {
            var RFC = document.getElementById('txtRFC');
            var Pwd = document.getElementById('txtContraseña');

            if (RFC.value === '' || Pwd.value === '') {
                alert('Faltan Datos');
                return;
            }

        }

    </script>


</head>
<body style="background-image:url('Images/FondoInicio.jpg');background-size: 100%; background-repeat: no-repeat; ">
    <form id="form1" runat="server">
        <div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="Images/logo.png" height="35" /></span>  Control de Pago a proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="active"><a href="Default.aspx">Iniciar Sesión</a></li>
                            <li><a href="SignUp.aspx">Registrarse</a></li>
                            <li><a href="ForgotPassword.aspx">Olvidé Contraseña</a></li>
                            <li><a href="#">Acerca de</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Sign In--> 
       <div class="row center-page">

           <h4><font color="white">Iniciar Sesión</font></h4>
           <hr/>
           <div class="col-md-1"></div>
           <div class="col-md-8">

            <asp:Label runat="server" ForeColor="White">RFC</asp:Label>
            <div>
                <asp:TextBox ID="txtRFC" style="width:60%" runat="server" Class="form-control" placeholder="Ingrese su RFC"></asp:TextBox>
            </div>
            <asp:Label  runat="server" ForeColor="White">Contraseña</asp:Label>
            <div>
                <asp:TextBox ID="txtContraseña" style=" width:80%" runat="server" Class="form-control CamposPage" placeholder="Ingrese su contraseña" TextMode="Password"></asp:TextBox>
            </div>
            
            <asp:CheckBox ID="cbkRecuerdame" runat="server"/>
            <asp:Label ID="Lblrecuerdame" runat="server" ForeColor="White" CssClass="control-label" Text="Recuérdame"></asp:Label>
               
            <div>
                <%--<asp:Button ID="btnSignUp" runat="server" class="btn btn-success" Text="Registrar" OnClick="btnSignUp_CLick" />--%>
                <button type="button" class="btn btn-md btn-success" runat="server" id="btnSignUp"  onserverclick="btnSignIn_ServerClick">
                    <span class="glyphicon glyphicon-log-in" aria-hidden="true"></span>&nbsp;&nbsp;Iniciar Sesión</button>
               <asp:Label ID="lblForgotPass" runat="server" ForeColor="White" Width="150px"><a style="color:#fff" href="ForgotPassword.aspx">Olvidé mi contraseña</a></asp:Label>
            </div>

           </div>
           <div class="col-md-3"></div>
       </div>
        <!--Sign In-->

    </form>
    </body>
<script src="Scripts/jquery-3.2.1.min.js"></script>
<script src="Scripts/bootstrap.min.js"></script>
<script src="Scripts/DataTables/jquery.dataTables.js"></script>
<script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
<script src="Scripts/jszip.min.js"></script>
<script src="Scripts/pdfmake/pdfmake.min.js"></script>
<script src="Scripts/pdfmake/vfs_fonts.js"></script>
<script src="Scripts/DataTables/dataTables.bootstrap.min.js"></script>
<script src="Scripts/DataTables/buttons.html5.min.js"></script>
<script src="Scripts/DataTables/buttons.print.min.js"></script>

</html>
