﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class RestorePassword : System.Web.UI.Page
{
    DL dl = new DL();
    DataTable dt = new DataTable();
    DataTable dt2 = new DataTable();
    string RFC;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!IsPostBack)
        {
            string UniqueId = Request.QueryString["ui"];

            if (UniqueId != null)
            {
                Session["UniqueId"] = UniqueId;
                dt = dl.GetForgotPassUUID(UniqueId);
                if (dt.Rows.Count < 1)
                {
                    Response.Redirect("~/Default.aspx");
                }

            }
            else
            {
                Response.Redirect("~/Default.aspx");
            }
        }

    }

    protected void btnRestorePass_ServerClick(object sender, EventArgs e)
    {
        if (txtContraseña.Text == "" || txtConfirmarContraseña.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltan Datos')", true);
            return;
        }

        if (txtContraseña.Text != txtConfirmarContraseña.Text)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Las contraseñas no coinciden.')", true);
            return;
        }

        dt2 = dl.SetNewUserPass(txtContraseña.Text, Session["UniqueId"].ToString());

        if (dt2.Rows.Count > 0)
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Se restableció correctamente la contraseña');window.location ='../Default.aspx';", true);
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ocurrió un problema al restablecer la contraseña.')", true);
        }
    }
}