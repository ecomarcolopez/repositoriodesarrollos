﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RestorePassword.aspx.cs" Inherits="RestorePassword" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Restablecer contraseña</title>

        <!-- Bootstrap -->
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/dataTables.bootstrap.min.css" rel="stylesheet" />
    <link href="Content/DataTables/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="css/Custom-Cs.css" rel="stylesheet"/>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--<script src="js/bootstrap.min.js"></script>-->
    
</head>
<body style="background-image:url('Images/FondoInicio.jpg');background-size: 100%; background-repeat: no-repeat; ">
    <form id="form1" runat="server">
        <div>
            <div class="navbar navbar-default navbar-fixed-top" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="Default.aspx"><span>
                            <img alt="Logo" src="Images/logo.png" height="35" /></span>  Control de Pago a proveedores</a>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="Default.aspx">Iniciar Sesión</a></li>
                            <li><a href="SignUp.aspx">Registrarse</a></li>
                            <li class="active"><a href="#">Restaurar Contraseña</a></li>
                            <li><a href="#">Acerca de</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <!--Forgot Password--> 
       <div class="row center-page">

            <h4><font color="white">Restablecer contraseña</font></h4>
           <hr/>

           <div class="col-md-1"></div>
           <div class="col-md-8">
               <asp:Label id="lblContraseña" runat="server" ForeColor="White">Contraseña</asp:Label>
                <div>
                    <asp:TextBox ID="txtContraseña" Style="width: 80%" runat="server" Class="form-control CamposPage" placeholder="Ingrese su contraseña" TextMode="Password"></asp:TextBox>
                </div>

                <asp:Label id="lblConfirmarContraseña" runat="server" ForeColor="White">Confirmar Contraseña</asp:Label>
                <div>
                    <asp:TextBox ID="txtConfirmarContraseña" Style="width: 80%" runat="server" Class="form-control CamposPage" placeholder="Confirmar contraseña" TextMode="Password"></asp:TextBox>
                </div>

               <br />
                    <%--<asp:Button ID="btnSignUp" runat="server" class="btn btn-success" Text="Registrar" OnClick="btnSignUp_CLick" />--%>
                    <button type="button" class="btn btn-md btn-success" runat="server" id="btnRestorePass" onserverclick="btnRestorePass_ServerClick" visible="True">
                        <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&nbsp;Restaurar</button>
                    <asp:Label ID="lblMensaje" runat="server" ForeColor="White" Width="487px"></asp:Label>
                <div>
            
           <div class="col-md-3"></div>
       </div>
        <!--Forgot Password-->

    </form>
    </body>
    <script src="Scripts/jquery-3.2.1.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
    <script src="Scripts/DataTables/jquery.dataTables.js"></script>
    <script src="Scripts/DataTables/dataTables.buttons.min.js"></script>
    <script src="Scripts/jszip.min.js"></script>
    <script src="Scripts/pdfmake/pdfmake.min.js"></script>
    <script src="Scripts/pdfmake/vfs_fonts.js"></script>
    <script src="Scripts/DataTables/dataTables.bootstrap.min.js"></script>
    <script src="Scripts/DataTables/buttons.html5.min.js"></script>
    <script src="Scripts/DataTables/buttons.print.min.js"></script>

    <script>

        function ValidaEmail(emailToEvaluate) {

            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if (re.test(emailToEvaluate)) {

                return true;
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {

        $('#txtEmail').blur(function () {


            var txtRFC = document.getElementById('txtRFC');
            var txtEmail = document.getElementById('txtEmail');

            txtEmail.value = txtEmail.value.trim();
            txtRFC.value = txtRFC.value.trim();

            var Parametro = "{RFC:'" + txtRFC.value + "',Email:'" + txtEmail.value + "'}";

            

            $.ajax({
                type: "POST",
                url: "ForgotPassword.aspx/ValidaEmailRFC",
                data: Parametro,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var result = response.d;

                    if (result === "Si") {
                        txtEmail.setAttribute("style", "background-color:#89e5ac;width:60%");
                        $('#txtEmail').popover("hide");
                        document.getElementById("btnForgotPassword").disabled = false;
                    }
                    else {
                        txtEmail.setAttribute("style", "background-color:rgb(229, 123, 96);width:60%");
                        $('#txtEmail').popover("show");
                        document.getElementById("btnForgotPassword").disabled = true;

                    }

                },
                failure: function (response) {
                    alert(response.d);
                }

            });
                
               
                $('#txtEmail').popover({
                    trigger: 'manual',
                    placement: 'right',
                    content: function () {
                        var message = "Correo no encontrado en base de datos";
                        return message;
                    }
                });
        });



        $('#txtRFC').blur(function () {
                var txtRFC = document.getElementById('txtRFC');
                var Parametro = "{RFC:'" + txtRFC.value + "'}";

                $.ajax({
                    type: "POST",
                    url: "ForgotPassword.aspx/ConsultarRFC",
                    data: Parametro,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var result = response.d;

                        if (result === "Si") {
                            txtRFC.setAttribute("style", "background-color:#89e5ac;width:60%");
                            $('#txtRFC').popover("hide");
                            document.getElementById("btnForgotPassword").disabled = false;
                        }
                        else {
                            txtRFC.setAttribute("style", "background-color:rgb(229, 123, 96);width:60%");
                            $('#txtRFC').popover("show");
                            document.getElementById("btnForgotPassword").disabled = true;

                        }

                    },
                    failure: function (response) {
                        alert(response.d);
                    }


                });

                $('#txtRFC').popover({
                    trigger: 'manual',
                    placement: 'right',
                    content: function () {
                        var message = "Tu RFC no existe en nuestra base de datos.";
                        return message;
                    }
                });

            });
        });
        
        
    </script>
</html>


