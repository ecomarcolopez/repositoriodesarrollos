﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.Services;
using System.Data;

public partial class SignUp : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
 
    [WebMethod]
    public static object ConsultarRFC(string RFC)
    {
        DL dl = new DL();
        DataTable dt = new DataTable();
        dt = dl.GetRFCExistente(RFC);
    
        if(dt.Rows.Count>0)
        {
            return "Si";
        }
        else
        {
            return "No";
        } 
    }

    [WebMethod]
    public static object SignUpMethod(string RFC,string Pwd, string Email)
    {
        DL dl = new DL();
        DataTable dt = new DataTable();

        dt = dl.SetNewUser(RFC, Pwd, Email);

        string resul = "";

        if(dt.Rows.Count >0)
        {
            resul = dt.Rows[0][0].ToString();
        }

        return resul;
    }

    [WebMethod]
    public static object ValidaEmailRFC(string RFC,string Email)
    {
        DL dl = new DL();
        DataTable dt = new DataTable();

        dt = dl.ValidaEmailRFC(RFC, Email);
        BL bl = new BL();

        if (dt.Rows.Count > 0)
        {
            if (bl.IsValidEmail(Email))
            {
                return "Si";
            }
            else
            {
                return "No";
            }

        }
        else
        {
            return "No";
        }
    }
}