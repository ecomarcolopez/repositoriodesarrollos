﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Drawing;
using System.Web.Services;
using System.Data;

public partial class ForgotPassword : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }


    [WebMethod]
    public static object ConsultarRFC(string RFC)
    {
        DL dl = new DL();
        DataTable dt = new DataTable();
        dt = dl.GetRFCExistente(RFC);

        if (dt.Rows.Count > 0)
        {
            return "Si";
        }
        else
        {
            return "No";
        }
    }


    [WebMethod]
    public static object ValidaEmailRFC(string RFC, string Email)
    {
        DL dl = new DL();
        DataTable dt = new DataTable();

        dt = dl.ValidaEmailRFC(RFC, Email);

        BL bl = new BL();


        if (dt.Rows.Count > 0)
        {
            if (bl.IsValidEmail(Email))
            {
                return "Si";
            }
            else
            {
                return "No";
            }

        }
        else
        {
            return "No";
        }
    }



    protected void btnbtnConfirm_ServerClick(object sender, EventArgs e)
    {
        if (txtRFC.Text == "" || txtEmail.Text == "")
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Faltan Datos')", true);
        }

        DL dl = new DL();
        DataTable dt = new DataTable();
        dt = dl.GetRFCAndEmail(txtRFC.Text, txtEmail.Text);

        if (dt.Rows.Count > 0)
        {
            if (!string.IsNullOrWhiteSpace(dt.Rows[0][0].ToString()))
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Se envió un correo de confirmación, favor de verificar su bandeja de entrada.');window.location ='../Default.aspx';", true);
                //Response.Redirect("~/Default.aspx");
            }
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Ocurrió un problema al querer enviar el correo.')", true);
        }

    }
}